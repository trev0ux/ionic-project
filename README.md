
![image](/Yoolo.png)

# PROJETO YOOLO
[![NPM version][npm-version-image]][npm-version-url]
[![npm version](https://badge.fury.io/js/%40angular%2Fcore.svg)](https://badge.fury.io/js/%40angular%2Fcore)
[![npm version](https://badge.fury.io/js/ts.svg)](https://badge.fury.io/js/ts)
[![NPM][npm-image]][npm-url]

  
[npm-image]: https://nodei.co/npm/getmodule.png?downloads=true&downloadRank=true&stars=true
[npm-url]: https://nodei.co/npm/getmodule/
[npm-version-image]: https://badge.fury.io/js/getmodule.svg?style=flat
[npm-version-url]: https://npmjs.org/package/getmodule
[npm-ts-image]: https://badge.fury.io/js/getmodule.svg?style=flat
[npm-ts-url]: https://npmjs.org/package/getmodule




>Este documento tem por finalidade destacar os materiais necessários para o desenvolvimento do Yoolo. Ele deve nortear o desenvolvimento do aplicativo.


[Guia de instalação do ambiente de desenvolvimento](https://docs.google.com/document/d/1RAxlFLqRPYynnNkytRlAYQvtFRnAmuio0KIIfMgcqNg/edit?usp=drivesdk)

[Documento de especificação de requisitos](https://docs.google.com/document/d/1-TtMGiHZ8DZsb7RsU9GAaAQYHeWBgDopSVntT24Z3sc/edit?usp=sharing)

---
> * Repositório: `GitLab` 
> * Arquitetura: `Angular JS, SASS, TypeScript e Node JS` 
> * Linguagem: `HTML, CSS e Java Script`
> * Framework: `Ionic 3` 
> * Database: `Mongo DB` 
---

## Legenda


               Tabela de Prioridades
|   Código  |          Nome        |       Descrição      |
|-----------|----------------------|----------------------|
|     O     |      Obrigatório     | Requisitos sem o qual o sistema não poderá entrar em funcionamento|
|     I     |      Importante      | Requisitos fundamentais, mas que não impedem que parte do sistema entre em funcionamento.                     | 
|     D     |       Desejável      | Requisitos que não impedem o funcionamento do sistema sem sua implementação, podendo ser contemplado posteriormente.|
|     *     |    Campo Obrigatório |    Ao ser informado ao lado do campo, indica a obrigatoriedade do mesmo.                  |
|     TM    |        Tamanho       | Refere-se à quantidade de caracteres permitidos no campo.|                  


# Instalação

## Fast Salon App Cliente

git clone git@gitlab.com:allandss/fast-salon-app.git



## Fast Salon App Profissional

git clone git@gitlab.com:allandss/fast-salon-app-professional.git



##  *Materiais de apoio para o desenvolvimento*

- [Guia prático de GIT](https://rogerdudler.github.io/git-guide/index.pt_BR.html)

- [Componentes do Ionic](https://ionicframework.com/docs/components)

- [Documentação SASS](https://sass-lang.com/documentation*)

- [Guia de como trabalhar com Angular JS](https://www.eusoudev.com.br/trabalhar-com-angularjs/)

- [Protótipo funcional no Figma](https://www.figma.com/file/svLYiSbeuqWRm8yyluHT8t/App-Yoolo-Club-Usu%C3%A1rio-cadastrado-Plano-de-assinatura?node-id=0%3A1*a)

- [Desenvolvendo RESTfull API com Node e TypeScript](https://gitlab.com/tiago.lopes/yoolo-club-api)

- [Criando chave SSH pública](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)

