import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PreLoaderPage } from './pre-loader.page';

describe('PreLoaderPage', () => {
  let component: PreLoaderPage;
  let fixture: ComponentFixture<PreLoaderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreLoaderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PreLoaderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
