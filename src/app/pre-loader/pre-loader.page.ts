import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pre-loader',
  templateUrl: './pre-loader.page.html',
  styleUrls: ['./pre-loader.page.scss'],
})
export class PreLoaderPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
