import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreLoaderPageRoutingModule } from './pre-loader-routing.module';

import { PreLoaderPage } from './pre-loader.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreLoaderPageRoutingModule
  ],
  declarations: [PreLoaderPage]
})
export class PreLoaderPageModule {}
